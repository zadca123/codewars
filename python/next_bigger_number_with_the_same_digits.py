#!/usr/bin/env python3


def next_bigger(n: int) -> int:
    n = list(str(n))

    i = len(n) - 2
    while i >= 0 and n[i] >= n[i + 1]:
        i -= 1
    if i < 0:
        return -1

    j = len(n) - 1
    while j > i and n[j] <= n[i]:
        j -= 1

    n[i], n[j] = n[j], n[i]
    n[i + 1 :] = sorted(n[i + 1 :])

    return int("".join(n))


# def next_bigger(n: int) -> int:
#     n: list[str] = list(str(n))
#     for i in range(len(n) - 1, -1, -1):
#         for j in range(i, i - 2, -1):
#             print(i, j)
#             if n[i] > n[j]:
#                 n[j], n[i] = n[i], n[j]
#                 n[j + 1 :] = sorted(n[j + 1 :])
#                 return int("".join(n))

#     return -1


print(next_bigger(204752))  # should be 205247
