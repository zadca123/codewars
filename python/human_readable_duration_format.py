#!/usr/bin/env python3

from datetime import datetime


def format_duration(seconds):
    ss = seconds % 60
    mm = int(seconds / 60) % 60
    hh = int(seconds / 3600) % 24
    dd = int((seconds / 3600) / 24) % 365
    yy = int(((seconds / 3600) / 24) / 365)

    result = ""
    print(dd, yy)
    if yy > 0:
        print("HERE")
        result = f"{yy} year"
        if yy > 1:
            result += "s"
    if dd > 0:
        if result:
            result += ", "
        result += f"{dd} day"
        if dd > 1:
            result += "s"

    if hh > 0:
        if result:
            result += ", "
        result += f"{hh} hour"
        if hh > 1:
            result += "s"
    if mm > 0:
        if result:
            result += ", "

        result += f"{mm} minute"
        if mm > 1:
            result += "s"
    if ss > 0:
        if mm > 0 or hh > 0:
            result += " and "

        result += f"{ss} second"
        if ss > 1:
            result += "s"

    return "now" if not result else result


print(format_duration(363343211332))
