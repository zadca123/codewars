#!/usr/bin/env python3
x = "(((())))"


def valid_parentheses(string):
    string = [char for char in string if char in "()"]

    while "(" in string or ")" in string:
        if string[0] == ")" or string[-1] == "(":
            return False
        if not string:
            return True

        for i, bracket in enumerate(string[1:]):
            prev = string[i]
            if prev + bracket == "()":
                string.pop(i)
                string.pop(i)
                break

    return not bool(string)


print(valid_parentheses("q()()()we()()()"))
