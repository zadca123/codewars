#!/usr/bin/env python3


decisions = {
    "1": "124",
    "2": "1235",
    "3": "236",
    "4": "1457",
    "5": "24568",
    "6": "3569",
    "7": "478",
    "8": "57890",
    "9": "689",
    "0": "08",
}


def get_pins(observed):
    if isinstance(observed, int):
        observed = str(observed)
    if not observed:
        return []
    if len(observed) == 1:
        return decisions[observed]

    result = []

    for first_digit in decisions[observed[0]]:
        for rest_digits in get_pins(observed[1:]):
            result.append(first_digit + rest_digits)

    return result


print(get_pins("123"))
