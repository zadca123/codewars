def fibonacci(n, cache={0: 0, 1: 1}):
    if n in cache:
        return n
    cache[n] = fibonacci(n - 1) + fibonacci(n - 2)
    return cache[n]


print(fibonacci(66))
