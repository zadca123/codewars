public class Counter {
    public int countSheeps(Boolean[] arrayOfSheeps) {
        int count = 0;
        for(Boolean sheep: arrayOfSheeps){
            if (sheep != null && sheep){
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        Boolean[] sheeps = {true,false,true,true};
        Counter counter = new Counter();
        int sheepCount = counter.countSheeps(sheeps);
        System.out.println(sheepCount);
    }
}
