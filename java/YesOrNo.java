class YesOrNo{
    public static String boolToWord(boolean b){
        return b ? "Yes" : "No";
    }
    public static void main(String[] args) {
        YesOrNo obj = new YesOrNo();
        String str = obj.boolToWord(true);
        System.out.println(str);
    }
}
