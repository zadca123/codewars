public class XO {
    public static boolean getXO (String str) {
        int count_x = 0, count_o = 0;
        for (char character : str.toLowerCase().toCharArray()) {
            if (character == 'x') count_x++;
            else if (character == 'o') count_o++;
        }
        return count_x == count_o;
    }

    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }
}
