import java.util.List;
import java.util.ArrayList;

public class ListFiltering {
    public static List<Integer> filterList(List<Object> list) {
        List<Integer> result = new ArrayList<>();
        for (Object element: list) {
            if (element instanceof Integer) {
                result.add((Integer) element);
            }
        }
        return result;
    }


    public static void main(String[] args) {
        ListFiltering obj = new ListFiltering();
        List<Object> mixedList = new ArrayList<>();
        mixedList.add(1);          // Integer
        mixedList.add(2);          // Integer
        mixedList.add(3);          // Integer
        mixedList.add(true);       // Boolean
        mixedList.add(false);      // Boolean
        mixedList.add("qwe");      // String
        mixedList.add("1");        // String
        System.out.println(obj.filterList(mixedList));
    }
}
